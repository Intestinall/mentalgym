import gym
import numpy as np
from matplotlib import pyplot as plt

from keras import Sequential
from keras.activations import linear, relu
from keras.layers import Dense
from keras.losses import mse
from keras.optimizers import adam
from utils.get_tendency import get_tendency
from utils.must_render import must_render
from utils.save_model import save_model

PLOT_EVERY = 10
RENDER_EVERY = float("Inf")
RENDER_COUNT = RENDER_EVERY


ENV_NAME = "BipedalWalker-v2"

env = gym.make(ENV_NAME)

STATE_SPACE = env.observation_space.shape[0]
try:
    ACTION_SPACE = env.action_space.n  # 4
except AttributeError:
    ACTION_SPACE = env.action_space.shape[0]

lr = 0.001


model = Sequential()
model.add(Dense(128, input_dim=STATE_SPACE, activation=relu))
model.add(Dense(128, activation=relu))
model.add(Dense(128, activation=relu))
model.add(Dense(ACTION_SPACE, activation=linear))
model.compile(loss=mse, optimizer=adam(lr=lr))

num_episodes = 10000

alpha = 1.0
gamma = 0.99
epsilon = 1.0
epsilon_min = 0.01
decay_factor = 0.999

rewards_over_time = []

for i in range(num_episodes):
    state = env.reset().reshape(-1, STATE_SPACE)

    epsilon *= decay_factor
    if epsilon < epsilon_min:
        epsilon = epsilon_min

    done = False
    current_reward = 0
    while not done:
        if np.random.random() < epsilon:
            action = np.random.uniform(size=ACTION_SPACE)
        else:
            action = model.predict(state)

        new_state, reward, done, _ = env.step(action)
        new_state = new_state.reshape(-1, STATE_SPACE)
        if must_render():
            env.render()

        if not RENDER_COUNT:
            env.render()

        current_reward += reward

        target = (alpha * reward) + (gamma * np.max(model.predict(new_state)))
        target_vec = model.predict(state)[0]
        target_vec[action] = target

        model.fit(state, target.reshape(-1, ACTION_SPACE), epochs=1, verbose=0)

        state = new_state

    RENDER_COUNT = RENDER_EVERY if not RENDER_COUNT else RENDER_COUNT - 1

    rewards_over_time.append(current_reward)

    save_model(model, ENV_NAME)

    if len(rewards_over_time) % PLOT_EVERY == 0:
        plt.plot(get_tendency(rewards_over_time, group_every=PLOT_EVERY))
        plt.ylabel("Reward")
        plt.xlabel(f"Generation (x{PLOT_EVERY})")
        plt.title(f"Generation = {i + 1}\nEpsilon = {epsilon}")
        plt.grid()
        plt.show()
