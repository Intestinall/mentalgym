from pathlib import Path

import tensorflow as tf


def load_model(model_name):
    root = Path(Path(__name__).parent.parent, Path("trained_models"))
    return tf.keras.models.load_model(Path(root, f"{model_name}.h5"))
