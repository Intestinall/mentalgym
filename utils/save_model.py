import os
from datetime import datetime
from pathlib import Path


def save_model(model):
    model_name = input("Enter model name : ")
    if model_name:
        now = datetime.now().isoformat()
        root = Path(Path(__name__).parent.parent, Path("trained_models"))
        model.save(Path(root, f"{model_name}_{now}.h5"))
