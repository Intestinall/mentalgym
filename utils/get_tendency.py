def get_tendency(l, group_every=10):
    if len(l) < group_every:
        return l

    t = []
    for i in range(0, len(l), group_every):
        sl = l[i : i + group_every]
        t.append(sum(sl) / len(sl))
    return t
