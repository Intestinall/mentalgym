def dqn_demonstration(dqn):
    user_response = input(
        "Would you like to see the trained model in action ? [y/N] "
    ).lower()

    if user_response in ("y", "yes"):
        while True:
            try:
                n = int(input("How many round ? : "))
                dqn.epsilon = 0.0
                dqn.play_n(n)
                break
            except ValueError:
                pass
