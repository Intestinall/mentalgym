import os
from pathlib import Path

import numpy as np
from dotenv import dotenv_values
from matplotlib import pyplot as plt
import tensorflow as tf
import random

from utils.get_tendency import get_tendency
import math


class MemoryReplay:
    def __init__(self, every=50, batch_size=40):
        self._memory = []
        self._every_count = 1
        self._every = every
        self._batch_size = batch_size

    @property
    def is_ready(self):
        if self._every_count % self._every == 0:
            self._every_count = 0
            return True
        else:
            return False

    def get_batch(self, dqn):
        sample = random.sample(self._memory, self._batch_size)
        states, target_vecs = np.empty(self._batch_size), np.empty([self._batch_size, dqn.env.action_space])
        for i, (state, action, reward, new_state) in enumerate(sample):
            states[i] = state
            target = dqn.compute_target(reward, new_state)
            target_vec = dqn.get_target_vec(new_state, action, target).reshape(-1, dqn.env.action_space)
            target_vecs[i] = target_vec

        return states, target_vecs

    def append(self, obj):
        self._every_count += 1
        self._memory.append(obj)


class DeepQNetwork:
    def __init__(
        self,
        env,
        model,
        alpha=1.0,
        gamma=0.99,
        epsilon=1.0,
        epsilon_min=0.01,
        decay_factor=0.999,
    ):
        """
        :param alpha:
        Reward maximizer:
            Determine the importance of current reward.
            The higher it is, the more the reward
            will be "rewarding"/"enjoyable", the more the punishment
            (negative reward) will be "punishing"/"rough".
        :param gamma:
        Future reward multiplier:
            Determine the importance of future rewards.
            The higher it is, the more the agent will be rewarded for possible
            future actions given the current action/state couple.
        :param epsilon:
        Percentage of random action:
            Determine the percentage of random action for each frame.
        :param epsilon_min:
        Minimum percentage of random action:
            Lower bound of epsilon.
        :param decay_factor:
           TODO
        """
        self.env = env
        self.model = model
        self.alpha = alpha
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.decay_factor = decay_factor
        self.memory_replay = MemoryReplay()
        self.rewards = []

    def play(self, render=False):
        state = self.env.reset()

        self.update_epsilon()

        must_render, must_plot, plot_every = self._get_env()
        must_render = render or must_render

        done = False

        counter = 0
        total_reward = 0

        while not done:
            counter += 1

            action = self.get_action(state)
            new_state, reward, done = self.env.step(action)

            target = self.compute_target(reward, new_state)

            target_vec = self.get_target_vec(new_state, action, target)

            self.memory_replay.append((state, action, reward, new_state))

            if self.memory_replay.is_ready:
                self.fit_model_batch()

            state = new_state
            total_reward += reward

            if must_render:
                self.env.render()

        self.rewards.append(total_reward)

        if must_plot and len(self.rewards) % plot_every == 0:
            self.plot(plot_every)

        return total_reward

    @staticmethod
    def _get_env():
        root = Path(__file__).parent.parent
        env = dotenv_values(stream=Path(root, ".env"))
        return (
            env.get("GYM_RENDER") == "1",
            env.get("PLOT_RENDER") == "1",
            int(env.get("PLOT_EVERY", 10)),
        )

    def update_epsilon(self):
        self.epsilon *= self.decay_factor
        if self.epsilon < self.epsilon_min:
            self.epsilon = self.epsilon_min

    def get_action(self, state):
        """
        Epsilon represent the percentage of random action.

        np.argmax return the index of the max.
        """
        if np.random.random() < self.epsilon:
            return np.random.randint(0, self.env.action_space)
        else:
            return np.argmax(self.model.predict(state))

    def compute_target(self, raw_reward, new_state):
        reward = self.alpha * raw_reward
        max_possible_predicted_reward = np.max(self.model.predict(new_state))
        return reward + (self.gamma * max_possible_predicted_reward)

    def get_target_vec(self, state, action, target):
        target_vec = self.model.predict(state)[0]
        target_vec[action] = target
        return target_vec

    def fit_model_batch(self):
        states, target_vecs = self.memory_replay.get_batch(self)
        self.model.fit(
            states,
            target_vecs,
            epochs=1,
            verbose=0,
            batch_size=40
        )

    def fit_model(self, state, target_vec):
        self.model.fit(
            state,
            target_vec.reshape(-1, self.env.action_space),
            epochs=1,
            verbose=0,
        )

    def plot(self, plot_every):
        plt.plot(get_tendency(self.rewards, group_every=plot_every))
        plt.ylabel("Reward")
        plt.xlabel(f"Generation (x{plot_every})")
        plt.title(f"Epsilon = {self.epsilon}")
        plt.grid()
        plt.show()
