import gym
import numpy as np
from gym.spaces import Discrete, Box


class Env:
    def __init__(self, name):
        self._env = gym.make(name)
        self.observation_space = self._get_observation_space()
        self.action_space = self._get_action_space()
        pass

    def _get_action_space(self):
        space = self._env.action_space
        if isinstance(space, Discrete):
            return space.n
        elif isinstance(space, Box) and len(space.shape) == 1:
            return space.shape[0]
        else:
            raise NotImplemented("Support for Discrete or Box(1) only")

    def _get_observation_space(self):
        space = self._env.observation_space
        if isinstance(space, Discrete):
            return 1
        elif isinstance(space, Box) and len(space.shape) == 1:
            return space.shape[0]
        else:
            raise NotImplemented("Support for Discrete or Box(1) only")

    def _normalize_state(self, state):
        if not isinstance(state, np.ndarray):
            state = np.array([state])
        return state.reshape(-1, self.observation_space)

    def reset(self):
        return self._normalize_state(self._env.reset())

    def step(self, action):
        new_state, reward, done, _ = self._env.step(action)
        new_state = self._normalize_state(new_state)
        return new_state, reward, done

    def render(self):
        return self._env.render()
