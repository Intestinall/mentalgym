import threading
import time

import tensorflow as tf  # isort:skip

from utils.load_model import load_model
from utils.post import dqn_demonstration
from utils.save_model import save_model

tf.compat.v1.disable_eager_execution()

from tensorflow.keras import Sequential
from tensorflow.keras.activations import linear, relu
from tensorflow.keras.layers import Dense
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam

from env.env import Env
from algorithm.dqn import DeepQNetwork
from utils.tf import set_gpu_growing


set_gpu_growing(tf)


EPOCHS = 10000

# env = Env("CartPole-v0")
# env = Env("LunarLander-v2")
env = Env("NChain-v0")

model = Sequential()
model.add(Dense(512, input_dim=env.observation_space, activation=relu))
model.add(Dense(512, activation=relu))
model.add(Dense(env.action_space, activation=linear))
model.compile(loss=mse, optimizer=Adam(lr=0.001))

dqn = DeepQNetwork(
    env,
    model,
    decay_factor=0.9
)


try:
    for i in range(EPOCHS):
        reward = dqn.play()
        print(f"Epoch {i} : Reward = {reward}, Epsilon = {dqn.epsilon:.3f}")
except KeyboardInterrupt:
    pass
# finally:
#     save_model(model)
#     dqn_demonstration(dqn)
